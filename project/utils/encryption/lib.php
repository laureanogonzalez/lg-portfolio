<?php
	define("STATICKEY", "cd1afb7abafb1cd6b5324061ab208cd12abe0aefdebae5e1d417e2ffb2a00a4");
	function encrypt($password){
		// Key for algorithm
		$key = pack('H*', STATICKEY);
		// initialization vector 
		$iv = md5(md5($key));

		$password_utf8 = utf8_encode($password);
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $password_utf8, MCRYPT_MODE_CBC, $iv);
		return base64_encode($ciphertext);
	}
	function decrypt($hashpwd){
		// Key for algorithm
		$key = pack('H*', STATICKEY);
		// initialization vector 
		$iv = md5(md5($key));

		$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($hashpwd), MCRYPT_MODE_CBC, $iv);
		return rtrim($plaintext, "");
	}
?>