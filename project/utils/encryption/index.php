<!DOCTYPE html>
<html>
<head>
	<title>Rijndael 256-bit encryption/decryption + Salt</title>
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
	<meta name="robots" content="noindex, nofollow">
	<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<style type="text/css">
		@import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

		.box {
			border-radius: 3px;
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			padding: 10px 25px;
			text-align: right;
			display: block;
			margin-top: 60px;
		}
		.box h2{ text-align: center; }
		.box-icon {
			background-color: #57a544;
			border-radius: 50%;
			display: table;
			height: 100px;
			margin: 0 auto;
			width: 100px;
			margin-top: -61px;
		}
		.box-icon span {
			color: #fff;
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
		.info h4 {
			font-size: 26px;
			letter-spacing: 2px;
			/*text-transform: uppercase;*/
		}
		.info > p {
			color: #717171;
			font-size: 16px;
			padding-top: 10px;
			text-align: justify;
		}
		.info > a {
			background-color: #03a9f4;
			border-radius: 2px;
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			color: #fff;
			transition: all 0.5s ease 0s;
		}
		.info > a:hover {
			background-color: #0288d1;
			box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.12);
			color: #fff;
			transition: all 0.5s ease 0s;
		}
	</style>
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Rijndael 256-bit encryption/decryption + Salt</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		</div>
	</div>
</nav>
	<div class="container">

		<div class="row">
			<div id="encrypt" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 method" data-controller="encrypt.php">
				<div class="box">
					<div class="info">
					<h2>Encrypt</h2>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-9">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">Plain text</div>
										<input class="form-control" placeholder="Insert message..." />
									</div>
								</div>
							</div>
							<div class="col-sm-3 text-left">
								<button class="btn btn-primary btn-block">Encrypt</button>
							</div>
						</div>
					</div>
					<div class="output hide">
						<h4>Output</h4>
						<div class="alert alert-success" role="alert">...</div>
					</div>
					</div>
			   </div>
			</div>
			<div id="decrypt" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 method" data-controller="decrypt.php">
				<div class="box">
					<div class="info">
						<h2>Decryption</h2>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-9">
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-addon">Cypher</div>
											<input class="form-control" placeholder="Enger cypher..." />
										</div>
									</div>
								</div>
								<div class="col-sm-3 text-left">
									<button class="btn btn-primary btn-block">Decrypt</button>
								</div>
							</div>
							<div class="output hide">
								<h4>Output</h4>
								<div class="alert alert-success" role="alert">...</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<script type="text/javascript">
	$('.method button').click(function(){
		var modal = $(this).parents('.method');
		var _text = modal.find('input').val();
		$.ajax({
			url: modal.data('controller'),
			data: {text: _text},
			success: function(output){
				modal.find('.output').removeClass('hide');
				modal.find('.alert').text(output);
			}
		});
	});
	</script>
</body>
</html>