<!-- Footer -->
<table width="580" class="deviceWidthFull" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse;">
   <tr>
      <td bgcolor="#cf5d5d" style="padding:10px 0; color: #ffffff">
         <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="border-collapse: collapse;">
            <tr>
               <td>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth" style="border-collapse: collapse; margin-top: 10px; max-width: 275px;">
                     <tr>
                        <td>
                           <table border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="border-collapse: collapse;">
                              <tr>
                                 <td>
                                    <a href="<?php echo $options['facebook-url']; ?>" style="float: left; margin-left: 6px; padding-left: 10px"><img src="<?php echo $imageDir; ?>social-facebook.png"></a>
                                    <a href="<?php echo $options['twitter-url']; ?>" style="float: left; margin-left: 6px; "><img src="<?php echo $imageDir; ?>social-twitter.png"></a>
                                    <a href="<?php echo $options['youtube-url']; ?>" style="float: left; margin-left: 6px; "><img src="<?php echo $imageDir; ?>social-youtube.png"></a>
                                    <span style="font-size: 11px; padding: 12px 15px 0 10px; float: left;color: #ffffff" valign="bottom">Conéctate con nosotros</span>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
                  <!-- [if !mso]>
                  <table cellpadding="0" cellspacing="0" border="0" align="left" class="deviceWidth" style="border-collapse: collapse; margin-top: 10px; display:none; overflow:hidden; float:left; display:none; line-height:0px; mso-hide:all;">
                     <tr>
                        <td>
                           <table border="0" align="right" cellpadding="0" cellspacing="0" class="deviceWidth" style="border-collapse: collapse;">
                              <tr>
                                 <td style="padding-left: 15px">
                                    <span style="float: left; font-size: 11px; padding: 12px 8px 0 0;color: #ffffff" valign="middle">Download our app</span>
                                    <a href="#" style="float: left; margin-left: 6px"><img src="<?php echo $imageDir; ?>download-apple.png"></a>
                                    <a href="#" style="float: left; margin-left: 6px"><img src="<?php echo $imageDir; ?>download-google.png"></a>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </table>
                  <![endif]-->
               </td>
            </tr>
         </table>
         <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="border-collapse: collapse;">
            <tr>
               <td>
                  <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="border-collapse: collapse;">
                     <tr>
                        <td style="font-size: 10px;padding: 15px;line-height: 12px;color: #ffffff">
                           Este correo electrónico fue enviado, ya que usted se suscribió a nuestra lista de distribución. Por favor, tenga en cuenta que si ha cancelado su suscripción previamente de comecasero.com, ya no recibirá boletines u ofertas especiales. Sin embargo, usted seguirá recibiendo notificaciones acerca de su cuenta. Para asegurarse de que usted va a recibir correos electrónicos de nosotros, por favor, agregue <a style="color: #fff" href="mailto:info@comecasero.com">info@comecasero.com</a> a su libreta de direcciones.
                           <br><br>
                           <a style="color: #fff" href="<?php echo $options['privacyPolicy']; ?>">Política de privacidad</a>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
<table width="580" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; display:none; overflow:hidden; float:left; display:none; line-height:0px; mso-hide:all;">
   <tr>
      <td>%%EID%%-%%SITEID%%-%%EMAILDEFAULTSITEID%%</td>
   </tr>
</table>