<?php
  $imageDir = "images/";
  $mediaQueries = true;

  $defaultOptions = array(
    "facebook-url" => "#",
    "twitter-url" => "#",
    "youtube-url" => "#",
    "logoUrl" => "",
    "initialText" => "",
    "privacyPolicy" => "",
  );
?>
<!--[if mso]>

<style>
    table, td, div, span {
      font-family: Helvetica, serif !important;
    }
</style>

<![endif]-->
<style type="text/css">
   @media only screen and (max-width: 640px)  {
      body[yahoo] .deviceWidth {width:440px!important; padding:0;}
      body[yahoo] .deviceWidthFull {width:100%!important; padding:0;}
      .hideOnDesktop{display: none!important;}
      .showOnDesktop{display: none!important;}
   }
   @media only screen and (max-width: 479px) {
      body[yahoo] .deviceWidth {width:320px!important; padding:0;}    
   }
</style>