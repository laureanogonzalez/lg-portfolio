<!-- Get started -->
<table width="580" class="deviceWidthFull" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse;" bgcolor="#ffffff">
   <tr>
      <td style="padding:10px 0px;">
         <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse;">
            <tr>
               <td width="100%" id="getStarted" style="font-size: 9px; color: #707070; font-weight: light; text-align: right; font-family: Helvetica; vertical-align: middle; padding-right:10px;">
               <?php echo $options["initialText"]; ?>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>

<!-- Header -->
<table width="580" class="deviceWidthFull" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed" style="border-collapse: collapse;">
   <tr>
      <td valign="top" style="height: 88px;border-bottom: 5px solid #000" bgcolor="#cf5d5d">
         <table width="100%" height="90" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
             <tr>
                 <td width="50%" valign="middle" style="padding:0 0 0 10px;">
                     <a href="<?php echo $options['logoUrl']; ?>"><img src="<?php echo $imageDir; ?>logo.png" alt="" border="0" align="left" style="width: 100%;max-width: 154px;"></a>
                 </td>
                 <td id="membershipID" width="50%" valign="middle" style="padding:20px 20px 0 0; color: #ffffff; font-size: 13px; font-weight: 400; text-align: right">
                     Los mejores platos cerca de ti
                 </td>
             </tr>
         </table>             
      </td>
   </tr>
</table>
