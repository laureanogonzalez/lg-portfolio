<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <?php include("views/main-includes.php"); ?>
      <?php 
         $options = array(
            "title" => "Newsletter - Juanma Durand",
            "facebook-url" => "#",
            "twitter-url" => "#",
            "youtube-url" => "#",
            "logoUrl" => "#",
            "initialText" => "Tienes hambre? <a href=\"#\" style=\"color: #006699;text-decoration: underline;\">Pide ya.</a>",
            "privacyPolicy" => "#"
         );

         $options =  array_merge($defaultOptions, $options);
      ?>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title><?php echo $options["title"]; ?></title>
   </head>
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Helvetica;width: 100%;background-color: #ffffff;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse;">
         <tr>
            <td valign="top" bgcolor="#eaebea">
               
               <table class="deviceWidth" width="100%" style="max-width: 580px" align="center" border="0" cellpadding="0" cellspacing="0" >
                  <tr>
                     <td>
                        <?php include("views/header.php"); ?>
                        <div style="height:18px">&nbsp;</div><!-- spacer -->
                        
                        <!-- Welcome -->
                        <table width="100%" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffee7e" style="border-collapse: collapse;">
                           <tr>
                              <td align="center" style="padding: 20px 20px 30px 20px;">
                                 <a href="#"><img src="<?php echo $imageDir; ?>icon-clock.png" alt="" border="0"></a>
                                 <div style="padding: 10px 0; font-size: 24px; color: #636465; font-weight: bold">Pide comida por los mejores chefs de tu zona!</div>
                                 <div>
                                    <a href="#"><img src="<?php echo $imageDir; ?>button-checkupdates.png" style="margin: auto; display: block;"></a>
                                 </div>
                              </td>
                           </tr>
                        </table>

                        <div style="height:18px">&nbsp;</div><!-- spacer -->
                        
                        

                        <!-- Bullets -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#ffffff" style="border-collapse: collapse;">
                           <tr>
                              <td>
                                 <table width="290" height="327" align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="border-collapse: collapse;">
                                    <tr>
                                       <td style="padding: 20px 18px 20px 32px; ">
                                          <table style="border-collapse: collapse;">
                                             <tr>
                                                <td class="subheading" style="color: #006699;font-family: Helvetica;font-weight: bold;padding-bottom: 14px;font-size: 18px;">Pide comida para tu almuerzo, es muy fácil: </td>
                                             </tr>
                                             <tr>
                                                <td style="color: #636465;font-size: 15px;">
                                                   <table style="border-collapse: collapse;">
                                                      <tr>
                                                         <td colspan="2">Lo único que tienes que hacer es:</td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:15px 3px 0 0; vertical-align:top;">1.</td>
                                                         <td style="padding: 13px 0 0;">Entra a ComeCasero<sup style="font-size: 11px;">®</sup></td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:13px 3px 0 0; vertical-align:top;">2.</td>
                                                         <td style="padding: 13px 0 0;">Escoge tu plato. No te preocupes, nosotros seleccionamos los chefs más cercanos por ti.</td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding:13px 3px 0 0; vertical-align:top;">3.</td>
                                                         <td style="padding: 13px 0 0;">Ingresa tu dirección y listo! Tu comida llegará según lo pautado con el cocinero.</td>
                                                      </tr>
                                                      <tr>
                                                         <td colspan="2" style="padding: 15px 0 0;">
                                                            <a href="#" style="color: #006699;text-decoration: underline;margin-left: 16px;">Pidelo ya!</a>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                 <table width="290" height="327" align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="border-collapse: collapse;" bgcolor="#dddfde">
                                    <tr>
                                       <td>
                                          <a href="#">
                                             <img src="<?php echo $imageDir; ?>food1.jpg" style="display: block;max-width:100%; margin: auto;" height="327">
                                          </a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                        
                        <div style="height:18px">&nbsp;</div><!-- spacer -->

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#ffffff" style="border-collapse: collapse;">
                           <tr>
                              <td style="padding:32px 18px 24px 32px;">
                                 <table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                    <tr>
                                       <td class="subheading" style="color: #006699;font-family: Helvetica;font-weight: bold;padding-bottom: 14px;font-size: 18px;">Empieza a vivir una vida sana.</td>
                                    </tr>
                                    <tr>
                                       <td class="common-text" style="color: #636465;font-size: 15px;">
                                          ¿Quieres tener un estilo de vida saludable y dejar de pasártela en el sillón todo el tiempo? Lo único que debes hacer es seguir un par de pasos que mejorarán tu salud de forma significativa.
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div style="height:24px">&nbsp;</div>
                                          <!-- BULLET -->
                                          <table style="border-collapse: collapse;">
                                             <tr>
                                                <td valign="top" style="padding:0 20px 0px 0">
                                                   <img src="<?php echo $imageDir; ?>icon-salad.png" alt="" border="0" align="left">
                                                </td>
                                                <td class="common-text" valign="top" style="color: #636465;font-size: 15px;">
                                                   <div>
                                                   Escoge alimentos saludables.
                                                   <a href="#" class="common-link" style="padding-top: 5px; display: block; color: #006699;text-decoration: underline;">Encuentra los alimentos más saludables aquí.</a>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <div style="height:21px">&nbsp;</div>
                                          <!-- BULLET -->
                                          <table style="border-collapse: collapse;">
                                             <tr>
                                                <td valign="top" style="padding:0 20px 0px 0">
                                                   <img src="<?php echo $imageDir; ?>icon-chicken.png" alt="" border="0" align="left">
                                                </td>
                                                <td class="common-text" valign="top" style="color: #636465;font-size: 15px;">
                                                   <div>Come grasas saludables con moderación.</div>
                                                   <a href="#" class="common-link" style="padding-top: 5px; display: block; color: #006699;text-decoration: underline;">¿Por qué las grasas son malas?</a>
                                                </td>
                                             </tr>
                                          </table>
                                          <div style="height:21px">&nbsp;</div>
                                          <!-- BULLET -->
                                          <table style="border-collapse: collapse;">
                                             <tr>
                                                <td valign="top" style="padding:0 20px 0px 0">
                                                   <img src="<?php echo $imageDir; ?>icon-grapes.png" alt="" border="0" align="left">
                                                </td>
                                                <td class="common-text" valign="top" style="color: #636465;font-size: 15px;">
                                                   <div>Selecciona los alimentos que tengan poca azúcar y carbohidratos altamente refinados.</div>
                                                   <a href="#" class="common-link" style="padding-top: 5px; display: block; color: #006699;text-decoration: underline;">¿Cómo diferenciarlos?</a>
                                                </td>
                                             </tr>
                                          </table>
                                          <div style="height:21px">&nbsp;</div>
                                          <!-- BULLET -->
                                          <table style="border-collapse: collapse;">
                                             <tr>
                                                <td valign="top" style="padding:0 20px 0px 0">
                                                   <img src="<?php echo $imageDir; ?>icon-icecream.png" alt="" border="0" align="left">
                                                </td>
                                                <td class="common-text" valign="top" style="color: #636465;font-size: 15px;">
                                                   <div>Come una variedad de alimentos completos en lugar de comidas procesadas.</div>
                                                   <a href="#" class="common-link" style="padding-top: 5px; display: block; color: #006699;text-decoration: underline;">Las 6 verdades acerca de las comidas procesadas.</a>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>

                        <div style="height:18px">&nbsp;</div><!-- spacer -->

                        
                        <table width="100%" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse;">
                           <tr>
                              <td>
                                 <a href="#"><img src="<?php echo $imageDir; ?>button-signin.jpg" style="margin: auto; display: block;"></a>
                              </td>
                           </tr>
                        </table>
                        
                        <div style="height:18px">&nbsp;</div><!-- spacer -->

                        <?php include("views/footer.php"); ?>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>