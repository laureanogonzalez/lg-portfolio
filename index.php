<?php
// Is Local?
$whitelist = array('127.0.0.1','::1');
$isLocal = in_array($_SERVER['REMOTE_ADDR'], $whitelist);
$buildNumber = 2;
$projects = [
    [
        'title' => 'Flatboard',
        'new' => true,
        'description' => 'A React.js, ES6, Redux, Webpack project for practice and intends to show my skills in this new technology.',
        'thumbnail' => 'img/thumbs/flatboard.jpg',
        'url' => 'http://flatboard.heroku.com/',
        // 'disabled' => true,
        'github' => 'https://github.com/juanmadurand/flatboard',
    ],
    [
        'title' => 'BestBucket',
        'new' => true,
        'description' => 'Chrome extension for Bitbucket pull requests',
        'thumbnail' => 'https://lh3.googleusercontent.com/cLJnlkRkGy3XQN4P0ektgqhygrqCh4a99KQL1133Ch0kNzIzKJMIjEMKZrrHeifjpx2-G9su9Hk=s640-h400-e365-rw',
        'url' => 'https://chrome.google.com/webstore/detail/bestbucket/jphlhacdafpjjdpgkfkbgooaedpfcppe',
        'github' => 'https://github.com/juanmadurand/bestbucket',
    ],
    [
        'title' => 'ComeCasero',
        'description' => 'Homemade food startup (500 Startups Mexico City - Batch #4)',
        'thumbnail' => 'img/thumbs/comecasero.jpg',
        'url' => 'http://comecasero.juanmadurand.com/',
        'year' => '2014',
    ],
    [
        'title' => 'Newsletters',
        'description' => 'Sample newsletter made from scratch used in my entrepreneurship ComeCasero.',
        'thumbnail' => 'img/thumbs/newsletter.jpg',
        'url' => 'newsletter/',
        'year' => '2014',
    ],
    [
        'title' => 'Ximple',
        'description' => 'Freelance job for Ximple Talent Remote Recruit',
        'thumbnail' => 'img/thumbs/ximple.jpg',
        'url' => 'http://www.ximplehr.com/',
        'year' => '2013',
    ],
    [
        'title' => 'Laboratorio',
        'description' => 'Simple website made for free for a laboratory at university when I was starting web development.',
        'thumbnail' => 'img/thumbs/laboratorio.png',
        'url' => 'laboratorio/',
        'year' => '2009',
    ],
];

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Portfolio - Juanma Durand</title>
        <meta name="description" content="Juanma Durand - " />
        <link href='/img/favicon-64.png' rel='shortcut icon'>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <link rel="stylesheet" href="img/bootstrap.min.css">
        <link rel="stylesheet" href="img/main.css?<?php echo $buildNumber; ?>" />
    </head>
    <body id="top">
        <header id="header">
            <a href="#" class="image avatar"><img src="img/avatar.jpg" alt="Juanma Durand" /></a>
            <h1><strong>Portfolio</strong></h1>
            To find more about me, please visit my Linkedin or contact me!<br />
            <footer id="footer">
                <ul class="icons">
                    <li><a href="https://www.linkedin.com/in/juanmadurand" class="icon icon-linkedin"><span class="label">Linkedin</span></a></li>
                    <li><a href="https://github.com/juanmadurand" class="icon icon-github"><span class="label">Github</span></a></li>
                    <li><a href="mailto:hi@juanmadurand.com" class="icon icon-paper-plane"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Juanma Durand</li>
                </ul>
            </footer>
        </header>
        <div id="main">
            <section id="two">
                <div class="row">
                    <?php
                    foreach ($projects as $project) {
                    $project = (object) $project;
                    $hasSourceCode = isset($project->github);
                    $year = isset($project->year) ? $project->year : false;
                    $sourceLink = $hasSourceCode ? $project->github : "javascript:void()";
                    ?>
                    <div class="col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-block">
                                <h2 class="card-title">
                                <a href="<?= $project->url ?>"><?= $project->title ?></a>
                                <?php if(isset($project->year)){?>
                                    <sup><span class="label label-default"><?= $year ?></span></sup>
                                <?php } ?>
                                </h2>
                                <?php if (isset($project->new) && $project->new) { ?>
                                    <div class="card-title-new">
                                        New
                                    </div>
                                <?php } ?>
                            </div>
                            <div>
                                <a href="<?= $project->url; ?>">
                                    <img
                                        src="<?= $project->thumbnail; ?>"
                                        alt="<?= strtolower(str_replace(' ', '-', $project->title)); ?>"
                                    />
                                </a>
                            </div>
                            <div class="card-block">
                                <p class="card-text"><?php echo $project->description; ?></p>
                                <div class="card-bottom">
                                    <?php if (isset($project->disabled) && $project->disabled) { ?>
                                    <div><small class="text-muted">Not available yet</small></div>
                                    <?php } else { ?>
                                    <a href="<?php echo $project->url; ?>" class="card-link">Check it out</a>
                                    <?php if ($hasSourceCode) { ?>
                                    <a href="<?=$sourceLink?>" class="card-link">Source code</a>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </section>
        </div>
    </body>
</html>